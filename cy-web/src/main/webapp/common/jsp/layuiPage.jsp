<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="${rc.contextPath}/statics/plugins/layui/css/layui.css" media="all">
<script src="${rc.contextPath}/statics/libs/jquery.min.js"></script>
<script src="${rc.contextPath}/statics/plugins/layer/layer.js"></script>
<script src="${rc.contextPath}/statics/plugins/layui/layui.js"></script>
<script src="${rc.contextPath}/statics/plugins/ztree/jquery.ztree.all.min.js"></script>
<script src="${rc.contextPath}/common/js/whole/common.js"></script>
<link rel="stylesheet" href="${rc.contextPath}/statics/css/font-awesome.min.css">
